# Don't enter password twice at Teamviewer GUI and one command with PowerShell profile

- check if you have a personal profile already in PowerShell
```
    Test-Path $profile => if not exist then create new  
    New-Item $profile -ItemType File -Force
    echo $profile
    notepad $profile
```

- define this function in $profile with Teamviewer password
```
function ft() {
    & 'C:\Program Files (x86)\TeamViewer\TeamViewer.exe' -i 389128205 -m fileTransfer –P {Teamviewr Password}
}
```

- Source the profile whenever changing $profile
```
. $profile or source $profile
```

- if any permission issues then get permission to source/run personal profile
```
    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
```

- Just hit ft in PowerShell !!!



# Export Live DB automatically using WSL cron and window Task scheduler

- Install Window subsystem Linux : https://docs.microsoft.com/en-us/windows/wsl/install
- Just type 'wsl' in window command 

- git clone inside Ubuntu
```
cd /mnt/c/inetpub/
git clone https://gitlab.com/Blee_aftermath/export-live-db-and-open-file-transfer-quickly.git tool
cd tool
```
- 'tool' folder has script, exported db and window batch 

- add crontab to run this mysqldump during the night
```
service cron status
service cron start ( If not running )
contrab -e
15      5       *       *       *       /mnt/inetpub/tool/aftermathDump.sh
// Besso run the mydqldump at 5:15 AM. change your slot. It takes around 8 min (10.2021)
```

- It will generate /mnt/c/inetpub/tool/db.sql everyday. Check if the file be created 

# Window section

- Next create window batch file. Sample already there
```
@ECHO OFF
mysql -h localhost -u root -p0000 aftermath < C:\inetpub\tool\db.sql
PAUSE
```

- Set window task scheduler after your croned Time
```
open 'task scheduler'
create basic task <= right top
Name : Mysql import from Aftermath sql ( or else )
Trigger : I set at 6:00 AM ( adjust )
Action as C:\inetpub\tool\import2Local.bat
You can search with running time for Edit at 'Active Tasks' section ( mid bottom ) > Properties ( right )
```

- It could be confusing because we use Linux cron and Window task scheduler
- The reason is only in Linux I could reach remote mysql. If you can remote connection in win command, It could be more simple.
- Enjoy






